import { getAddress } from "viem";
import { ReliquaryAutobribesV1 } from "./abi/ReliquaryAutobribesV1";

export const ADDRESSES = {
  mainnet: {
    USDC: getAddress("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48"),
    WETH: getAddress("0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2"),
    WBTC: getAddress("0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"),
    univ3: {
      Factory: getAddress("0x1F98431c8aD98523631AE4a59f267346ea31F984"),
    },
  },
  polygon: {
    "USDC.e": getAddress("0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174"),
    USDT: getAddress("0xc2132D05D31c914a87C6611C10748AEb04B58e8F"),
    WMATIC: getAddress("0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270"),
    WETH: getAddress("0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619"),
    WBTC: getAddress("0x1BFD67037B42Cf73acF2047067bd4F2C47D9BfD6"),
    RETRO: getAddress("0xBFA35599c7AEbb0dAcE9b5aa3ca5f2a79624D8Eb"),
    CASH: getAddress("0x5D066D022EDE10eFa2717eD3D79f22F949F8C175"),
    univ3: {
      Factory: getAddress("0x1F98431c8aD98523631AE4a59f267346ea31F984"),
      pools: {
        "MATIC-USDC.e": {
          "500": getAddress("0xA374094527e1673A86dE625aa59517c5dE346d32"),
        },
        "USDC.e-WETH": {
          "500": getAddress("0x45dDa9cb7c25131DF268515131f647d726f50608"),
        },
        "WBTC-USDC.e": {
          "500": getAddress("0xeEF1A9507B3D505f0062f2be9453981255b503c8"),
        },
      },
    },
    retro: {
      Factory: getAddress("0x91e1B99072f238352f59e58de875691e20Dc19c1"),
      pools: {
        "USDC.e-CASH": {
          "100": getAddress("0x619259F699839dD1498FFC22297044462483bD27"),
        },
        "CASH-RETRO": {
          "10000": getAddress("0xb47A07966cE6812702C0567d03725F1b37E27877"),
        },
      },
    },
  },
};

export const AUTOBRIBES_RETRO_CONTRACT = {
  address: getAddress("0x41022f426378c3f655d33b4248ecdb2577401445"),
  abi: ReliquaryAutobribesV1,
};
