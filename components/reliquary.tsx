import {
  TokenInfo,
  TokenPrice,
  computeApr,
  useALMTokenPrices,
  useActiveLiquidityManagerInfo,
  useTokenPrice,
} from "contracts/pricing";
import {
  Relic,
  ReliquaryInfo,
  ReliquaryPoolAggregatedInfo,
  ReliquaryPoolInfo,
  aggregateOwnerReliquaryInfo,
  aggregateReliquaryPoolsInfo,
  useCreateRelicAndDeposit,
  useDepositInRelic,
  useERC20Allowance,
  useERC20Approve,
  useERC20BalanceOf,
  useHarvestRelic,
  useOwnerRelicIds,
  useRelicIds,
  useRelics,
  useReliquaryInfo,
  useReliquaryPools,
  useReliquaryPoolsTokens,
  useUpdateRelicPosition,
  useWithdrawFromRelic,
} from "contracts/reliquary";
import {
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { range } from "utils/utils";
import { Address, formatUnits, getAddress, parseUnits } from "viem";
import { useAccount, useQueryClient, useToken } from "wagmi";
import { Block, Button, ExternalLink, Modal } from "./ui/ui";
import { SECONDS_PER_WEEK } from "utils/constants";
import { useBoolean } from "usehooks-ts";
import { useBlockExplorerUrl } from "contracts/hooks/useBlockExplorerUrl";

function isALMToken(address: Address) {
  return true; // In Turbo v1 all Reliquary pool tokens are ALM token but in v2 we'll have TRB
}

function getALMTokens(tokens?: TokenInfo[]): TokenInfo[] {
  return tokens === undefined
    ? []
    : tokens.filter((token) => isALMToken(token.address));
}

type ReliquaryContextProviderProps = {
  reliquaryInfo: ReliquaryInfo;
  rewardToken: TokenInfo;
  rewardTokenPrice: TokenPrice;
};
const ReliquaryContextProvider = ({
  reliquaryInfo,
  rewardToken,
  rewardTokenPrice,
  children,
}: PropsWithChildren<ReliquaryContextProviderProps>) => {
  const { data: pools } = useReliquaryPools(range(reliquaryInfo.poolLength));
  const { data: poolsTokens } = useReliquaryPoolsTokens(pools);
  const { data: almInfo } = useActiveLiquidityManagerInfo(
    getALMTokens(poolsTokens),
  );
  const almTokenPrices = useALMTokenPrices(almInfo ? almInfo : {});

  const rewardRate = parseFloat(
    formatUnits(reliquaryInfo.rewardRate, rewardToken.decimals),
  );
  const rewardRateUsd = rewardRate * rewardTokenPrice.value;

  const poolsAggregatedInfo = useMemo(
    () =>
      pools &&
      poolsTokens &&
      almTokenPrices &&
      aggregateReliquaryPoolsInfo(
        pools,
        poolsTokens,
        almTokenPrices,
        reliquaryInfo.totalAllocPoint,
      ),
    [pools, poolsTokens, almTokenPrices, reliquaryInfo],
  );

  const aggregatedInfo = useMemo(() => {
    if (poolsAggregatedInfo === undefined) {
      return undefined;
    }
    return {
      rewardRate,
      rewardRateUsd,
      tvlUsd: poolsAggregatedInfo
        .map((poolInfo) => poolInfo.balanceUsd || 0)
        .reduce((sum, current) => sum + current, 0),
    };
  }, [poolsAggregatedInfo, rewardRate, rewardRateUsd]);

  if (
    !pools ||
    !poolsTokens ||
    !almInfo ||
    !almTokenPrices ||
    !poolsAggregatedInfo ||
    !aggregatedInfo
  ) {
    return <></>;
  }

  const context = {
    reliquaryInfo,
    rewardToken,
    rewardTokenPrice,
    pools,
    poolsTokens,
    poolsTokenPrices: almTokenPrices,
    poolsAggregatedInfo,
    aggregatedInfo,
  };
  return (
    <ReliquaryContext.Provider value={context}>
      {children}
    </ReliquaryContext.Provider>
  );
};

type Reliquary = {
  reliquaryInfo: ReliquaryInfo;
  rewardToken: TokenInfo;
  rewardTokenPrice: TokenPrice;
  pools: ReliquaryPoolInfo[];
  poolsTokens: TokenInfo[];
  poolsTokenPrices: { [key: Address]: TokenPrice | undefined };
  poolsAggregatedInfo: ReliquaryPoolAggregatedInfo[];
  aggregatedInfo: AggregatedReliquaryInfo;
};

type AggregatedReliquaryInfo = {
  rewardRate: number;
  rewardRateUsd: number;
  tvlUsd: number;
};

export const ReliquaryContext = createContext({} as Reliquary);
export const useReliquary = () => {
  return useContext(ReliquaryContext);
};

type PoolUIInfo = {
  url: string;
  displayName: string;
  shortDisplayName: string;
};

const RETRO_ICHI_WETH_USDC05_CONTRACT = getAddress(
  "0x7c96bE1026A5461418C557f2CDECfD0F50244371",
);
const RETRO_ICHI_WMATIC_USDC05_CONTRACT = getAddress(
  "0x5Ef5630195164956d394fF8093C1B6964cb5814B",
);
const RETRO_GAMMA_WETH_USDC05_CONTRACT = getAddress(
  "0xe058e1FfFF9B13d3FCd4803FDb55d1Cc2fe07DDC",
);
const RETRO_GAMMA_WMATIC_USDC05_CONTRACT = getAddress(
  "0xBE4E30b74b558E41f5837dC86562DF44aF57A013",
);
const RETRO_GAMMA_USDC_CASH01_CONTRACT = getAddress(
  "0x64e14623CA543b540d0bA80477977f7c2c00a7Ea",
);
const RETRO_GAMMA_USDT_CASH01_CONTRACT = getAddress(
  "0xD24fBbAEd8A380293969Ed8c0CCb615E1b57d38F",
);

function getRetroDepositUrl(tokenA: Address, tokenB: Address) {
  return `https://app.retro.finance/liquidity/add?currencyA=${getAddress(
    tokenA,
  )}&currencyB=${getAddress(tokenB)}&rangeType=automatic`;
}

const poolTokenToUIInfo = {
  [RETRO_GAMMA_WETH_USDC05_CONTRACT]: {
    url: getRetroDepositUrl(
      "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
      "0x7ceb23fd6bc0add59e62ac25578270cff1b9f619",
    ),
    displayName: "Gamma WETH/USDC 0.05",
    shortDisplayName: "GAMMA-WETH/USDC05",
  },
  [RETRO_GAMMA_WMATIC_USDC05_CONTRACT]: {
    url: getRetroDepositUrl(
      "0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270",
      "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
    ),
    displayName: "Gamma WMATIC/USDC 0.05",
    shortDisplayName: "GAMMA-WMATIC/USDC05",
  },
  [RETRO_ICHI_WETH_USDC05_CONTRACT]: {
    url: getRetroDepositUrl(
      "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
      "0x7ceb23fd6bc0add59e62ac25578270cff1b9f619",
    ),
    displayName: "Ichi WETH/USDC 0.05",
    shortDisplayName: "ICHI-WETH/USDC05",
  },
  [RETRO_ICHI_WMATIC_USDC05_CONTRACT]: {
    url: getRetroDepositUrl(
      "0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270",
      "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
    ),
    displayName: "Ichi WMATIC/USDC 0.05",
    shortDisplayName: "ICHI-WMATIC/USDC05",
  },
  [RETRO_GAMMA_USDC_CASH01_CONTRACT]: {
    url: getRetroDepositUrl(
      "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
      "0x5d066d022ede10efa2717ed3d79f22f949f8c175",
    ),
    displayName: "Gamma USDC/CASH 0.01",
    shortDisplayName: "GAMMA-USDC/CASH01",
  },
  [RETRO_GAMMA_USDT_CASH01_CONTRACT]: {
    url: getRetroDepositUrl(
      "0x5D066D022EDE10eFa2717eD3D79f22F949F8C175",
      "0xc2132D05D31c914a87C6611C10748AEb04B58e8F",
    ),
    displayName: "Gamma USDT/CASH 0.01",
    shortDisplayName: "GAMMA-USDT/CASH01",
  },
} as { [key: string]: PoolUIInfo };

type AmountInputProps = {
  buttonText: string;
  value?: string;
  setValue: (value: string) => any;
  max: string;
  disabled?: boolean;
  onClick?: () => any;
};
const AmountInput = ({
  buttonText,
  value,
  setValue,
  max,
  disabled,
  onClick,
}: AmountInputProps) => {
  const isDisabled = useMemo(
    () =>
      value === undefined ||
      value === "" ||
      parseFloat(value) <= 0 ||
      parseFloat(value) > parseFloat(max) ||
      disabled,
    [value, max, disabled],
  );
  return (
    <div className="flex flex-row w-full gap-2">
      <div className="relative w-4/6">
        <input
          type="number"
          min={0}
          max={max}
          value={value}
          placeholder="AMOUNT"
          className="flex w-full h-16 rounded-md text-3xl px-2 text-black outline-none"
          onChange={(e) => setValue(e.target.value)}
        />
        <Button
          className="w-2/6 h-3/4 absolute top-2 right-2 bg-black px-2 text-3xl"
          onClick={() => setValue(max)}
        >
          MAX
        </Button>
      </div>
      <Button disabled={isDisabled} className="w-2/6" onClick={onClick}>
        {buttonText}
      </Button>
    </div>
  );
};

const chooseApproveOrOtherFromAllowance = (
  depositValue: string | undefined,
  depositText: string,
  setDepositText: Dispatch<SetStateAction<string>>,
  otherText: string,
  tokenDecimals: number,
  tokenAllowance: bigint,
) => {
  if (depositValue === undefined || depositValue == "") {
    depositText != otherText && setDepositText(otherText);
    return;
  }
  const amountToDeposit = parseUnits(depositValue, tokenDecimals);
  if (amountToDeposit > tokenAllowance) {
    depositText != "Approve" && setDepositText("Approve");
  } else {
    depositText != otherText && setDepositText(otherText);
  }
};

const displayTimer = (totalSeconds: number, short: boolean): string => {
  if (totalSeconds == 0) {
    return "-";
  }
  let remains = totalSeconds;
  const weeks = Math.floor(remains / 604800);
  remains = remains % 604800;
  const days = Math.floor(remains / 86400);
  remains = remains % 86400;

  const date = new Date(0);
  date.setSeconds(remains);

  const wDisplay =
    weeks > 0
      ? weeks + (short ? "W " : weeks == 1 ? " week, " : " weeks, ")
      : "";
  const dDisplay =
    days > 0 ? days + (short ? "D " : days == 1 ? " day, " : " days, ") : "";
  return wDisplay + dDisplay + date.toISOString().substr(11, 8);
};

const displayTimeLong = (totalSeconds: number): string => {
  return displayTimer(totalSeconds, false);
};

const displayTimerShort = (totalSeconds: number): string => {
  return displayTimer(totalSeconds, true);
};

const Timer = ({
  initialValue,
  increment,
  displayTimer,
}: {
  initialValue: number;
  increment: number;
  displayTimer: (totalSeconds: number) => string;
}) => {
  const [secondsToNextLevel, setSecondsToNextLevel] = useState(initialValue);

  useEffect(() => {
    const timer = setInterval(() => {
      setSecondsToNextLevel((value) => value + increment);
    }, 1000);
    return () => clearInterval(timer);
  }, [increment]);

  return (
    <span>{displayTimer(Math.max(Math.round(secondsToNextLevel), 0))}</span>
  );
};

type RelicUIProps = {
  info: RelicInfo;
  owner: Address;
};

const RelicUI = ({ info, owner }: RelicUIProps) => {
  const {
    reliquaryInfo,
    pools,
    poolsTokens,
    poolsAggregatedInfo,
    aggregatedInfo: reliquaryAggregatedInfo,
    poolsTokenPrices,
  } = useReliquary();

  const pool = pools[info.position.poolId];
  const poolUiInfo = poolTokenToUIInfo[pool.token];
  const tokenInfo = poolsTokens[info.position.poolId];
  const amountInRelic = formatUnits(info.position.amount, tokenInfo.decimals);
  const levelInfo = pool.levelsInfo[info.position.level];
  const poolAggregatedInfo = poolsAggregatedInfo[info.position.poolId];

  const { writeResult: harvestRelicResult } = useHarvestRelic(info.id, owner);
  const { writeResult: updateRelicPositionResult } = useUpdateRelicPosition(
    info.id,
  );

  const maturity = useMemo(
    () => Date.now() * 1e-3 - info.position.entry,
    [info],
  );
  const secondsToNextLevel = useMemo(() => {
    return info.position.level < pool.levelsInfo.length - 1
      ? pool.levelsInfo[info.position.level + 1].requiredMaturity - maturity
      : 0;
  }, [info, maturity, pool]);

  const tokenPrice = poolsTokenPrices[tokenInfo.address];
  const relicBalanceUsd = tokenPrice
    ? tokenPrice.value *
      parseFloat(formatUnits(info.position.amount, tokenInfo.decimals))
    : NaN;

  const apr = computeApr(
    poolAggregatedInfo.balanceUsd!,
    reliquaryAggregatedInfo.rewardRateUsd,
    poolAggregatedInfo.rewardShare,
  );

  return (
    <Block>
      <div className="flex md:flex-row sm:flex-col gap-2 w-full items-center">
        <div className="flex flex-col md:basis-72 flex-none">
          <p className="text-3xl">Relic #{info.id}</p>
          <p>
            <span className="underline">Pool</span>:{" "}
            <ExternalLink href={poolUiInfo.url}>
              {poolUiInfo.displayName}
            </ExternalLink>
          </p>
          <p className="underline">Pending Rewards</p>
          <p>{formatUnits(info.pendingReward, tokenInfo.decimals)}</p>
          <p className="underline">Amount</p>
          <p>{amountInRelic}</p>
          <p className="underline">TVL</p>
          <p>${relicBalanceUsd}</p>
          <p>
            <span className="underline">APR</span>: {apr.toFixed(2)}%
          </p>
          <p>
            <span className="underline">Level</span>: {info.position.level + 1}{" "}
            / {pool.levelsInfo.length}
          </p>
          <p>
            <span className="underline">Boost</span>: x{levelInfo.multiplier}
          </p>
          <p className="underline">Maturity</p>
          <p>
            <Timer
              initialValue={maturity}
              increment={1}
              displayTimer={displayTimeLong}
            />
          </p>
          {secondsToNextLevel > 0 ? (
            <>
              <p className="underline">Next level in</p>
              <p>
                <Timer
                  initialValue={secondsToNextLevel}
                  increment={-1}
                  displayTimer={displayTimeLong}
                />
              </p>
            </>
          ) : (
            <></>
          )}
        </div>
        <div className="flex flex-row gap-2 grow">
          <div className="flex flex-col gap-4 grow">
            <div className="flex flex-row gap-2 flex-none">
              <Button
                className="w-full"
                disabled={info.pendingReward === 0n}
                onClick={harvestRelicResult.write}
              >
                Claim
              </Button>
              <Button
                className="w-full"
                disabled={info.levelOnUpdate == info.position.level}
                onClick={updateRelicPositionResult.write}
              >
                Level up
              </Button>
            </div>
            <div className="flex flex-col gap-2 grow">
              <ApproveOrDepositInput
                relicId={info.id}
                tokenInfo={tokenInfo}
                owner={owner}
                reliquaryAddress={reliquaryInfo.address}
              />
              <WithdrawInput
                relicId={info.id}
                max={amountInRelic}
                tokenDecimals={tokenInfo.decimals}
              />
            </div>
          </div>
        </div>
      </div>
    </Block>
  );
};

type WithdrawInputProps = {
  relicId: number;
  max: string;
  tokenDecimals: number;
};

function WithdrawInput({ relicId, max, tokenDecimals }: WithdrawInputProps) {
  const [value, setValue] = useState<string | undefined>(undefined);
  const amount =
    value !== undefined
      ? parseUnits(value as string, tokenDecimals)
      : undefined;
  const { writeResult } = useWithdrawFromRelic(relicId, amount);

  return (
    <AmountInput
      max={max}
      value={value}
      setValue={setValue}
      buttonText="Withdraw"
      onClick={writeResult.write}
      disabled={!writeResult.write}
    />
  );
}

type ApproveOrDepositInputProps = {
  relicId: number;
  tokenInfo: TokenInfo;
  owner: Address;
  reliquaryAddress: Address;
};

function ApproveOrDepositInput({
  relicId,
  tokenInfo,
  owner,
  reliquaryAddress,
}: ApproveOrDepositInputProps) {
  const tokenBalanceResult = useERC20BalanceOf(tokenInfo.address, owner);
  const tokenAllowanceResult = useERC20Allowance(
    tokenInfo.address,
    owner,
    reliquaryAddress,
  );

  const [depositText, setDepositText] = useState("Deposit");
  const [depositValue, setDepositValue] = useState<string | undefined>(
    undefined,
  );

  useEffect(() => {
    if (tokenAllowanceResult.data === undefined) {
      return;
    }
    chooseApproveOrOtherFromAllowance(
      depositValue,
      depositText,
      setDepositText,
      "Deposit",
      tokenInfo.decimals,
      tokenAllowanceResult.data,
    );
  }, [
    depositText,
    setDepositText,
    depositValue,
    tokenInfo,
    tokenAllowanceResult,
  ]);

  const max =
    tokenBalanceResult.data !== undefined
      ? formatUnits(tokenBalanceResult.data, tokenInfo.decimals)
      : "0";
  const amount =
    depositValue !== undefined
      ? parseUnits(depositValue, tokenInfo.decimals)
      : undefined;

  const { writeResult: approveWriteResult } = useERC20Approve(
    tokenInfo.address,
    reliquaryAddress,
    amount,
  );
  const { writeResult: depositWriteResult } = useDepositInRelic(
    depositText == "Deposit" ? relicId : undefined,
    amount,
  );

  return (
    <AmountInput
      max={max}
      value={depositValue}
      setValue={setDepositValue}
      buttonText={depositText}
      onClick={
        depositText == "Deposit"
          ? depositWriteResult.write
          : approveWriteResult.write
      }
    />
  );
}

type ApproveOrCreateAndDepositInputProps = {
  tokenInfo: TokenInfo;
  poolId?: number;
  owner: Address;
  reliquaryAddress: Address;
};

function ApproveOrCreateAndDepositInput({
  tokenInfo,
  poolId,
  owner,
  reliquaryAddress,
}: ApproveOrCreateAndDepositInputProps) {
  const tokenBalanceResult = useERC20BalanceOf(tokenInfo.address, owner);
  const tokenAllowanceResult = useERC20Allowance(
    tokenInfo.address,
    owner,
    reliquaryAddress,
  );

  const [depositText, setDepositText] = useState("Deposit");
  const [depositValue, setDepositValue] = useState<string | undefined>(
    undefined,
  );

  useEffect(() => {
    if (tokenAllowanceResult.data === undefined) {
      return;
    }
    chooseApproveOrOtherFromAllowance(
      depositValue,
      depositText,
      setDepositText,
      "Create & Deposit",
      tokenInfo.decimals,
      tokenAllowanceResult.data,
    );
  }, [
    depositText,
    setDepositText,
    depositValue,
    tokenInfo,
    tokenAllowanceResult,
  ]);

  const max =
    tokenBalanceResult.data !== undefined
      ? formatUnits(tokenBalanceResult.data, tokenInfo.decimals)
      : "0";
  const amount =
    depositValue !== undefined
      ? parseUnits(depositValue, tokenInfo.decimals)
      : undefined;

  const { writeResult: approveWriteResult } = useERC20Approve(
    tokenInfo.address,
    reliquaryAddress,
    amount,
  );
  const { writeResult: depositWriteResult } = useCreateRelicAndDeposit(
    owner,
    poolId,
    depositText == "Create & Deposit" ? amount : undefined,
  );

  return (
    <AmountInput
      max={max}
      value={depositValue}
      setValue={setDepositValue}
      buttonText={depositText}
      onClick={
        depositText == "Create & Deposit"
          ? depositWriteResult.write
          : approveWriteResult.write
      }
    />
  );
}

type RelicInfo = Relic;

type RelicInfoUIProps = {
  info: RelicInfo;
};

const RelicInfoUI = ({ info }: RelicInfoUIProps) => {
  const {
    rewardToken,
    pools,
    poolsTokens,
    poolsAggregatedInfo,
    poolsTokenPrices,
    aggregatedInfo: reliquaryAggregatedInfo,
  } = useReliquary();
  const blockExporerUrl = useBlockExplorerUrl();
  const pool = pools[info.position.poolId];
  const poolUiInfo = poolTokenToUIInfo[pool.token];
  const tokenInfo = poolsTokens[info.position.poolId];
  const amountInRelic = formatUnits(info.position.amount, tokenInfo.decimals);
  const levelInfo = pool.levelsInfo[info.position.level];
  const poolAggregatedInfo = poolsAggregatedInfo[info.position.poolId];

  const tokenPrice = poolsTokenPrices[tokenInfo.address];
  const relicBalanceUsd = tokenPrice
    ? tokenPrice.value *
      parseFloat(formatUnits(info.position.amount, tokenInfo.decimals))
    : NaN;

  const apr = computeApr(
    poolAggregatedInfo.balanceUsd!,
    reliquaryAggregatedInfo.rewardRateUsd,
    poolAggregatedInfo.rewardShare,
  );

  const account = useAccount();
  const { writeResult: updateRelicPositionResult } = useUpdateRelicPosition(
    account.isConnected ? info.id : undefined,
  );

  const maturity = useMemo(
    () => Date.now() * 1e-3 - info.position.entry,
    [info],
  );
  return (
    <div className="flex flex-col md:basis-72 flex-none">
      <p className="text-3xl">Relic #{info.id}</p>
      {updateRelicPositionResult.write ? (
        <Button className="h-12" onClick={updateRelicPositionResult.write}>
          Update
        </Button>
      ) : (
        <></>
      )}
      <p>
        <span className="underline">Owner</span>:{" "}
        <ExternalLink href={`${blockExporerUrl}/address/${info.owner}`}>
          {info.owner.substring(0, 6)}...
          {info.owner.substring(info.owner.length - 2)}
        </ExternalLink>
      </p>
      <p>
        <span className="underline">Pool</span>:{" "}
        <ExternalLink href={poolUiInfo.url}>
          {poolUiInfo.displayName}
        </ExternalLink>
      </p>
      <p className="underline">Pending Rewards</p>
      <p>{formatUnits(info.pendingReward, rewardToken.decimals)}</p>
      <p className="underline">Amount</p>
      <p>{amountInRelic}</p>
      <p className="underline">TVL</p>
      <p>{relicBalanceUsd}</p>
      <p>
        <span className="underline">APR</span>: {apr.toFixed(2)}%
      </p>
      <p>
        <span className="underline">Level</span>: {info.position.level + 1} /{" "}
        {pool.levelsInfo.length}
      </p>
      <p>
        <span className="underline">Boost</span>: x{levelInfo.multiplier}
      </p>
      <p className="underline">Maturity</p>
      <p>
        <Timer
          initialValue={maturity}
          increment={1}
          displayTimer={displayTimerShort}
        />
      </p>
    </div>
  );
};

type PoolUIProps = {
  info: ReliquaryPoolInfo;
  aggregatedInfo: ReliquaryPoolAggregatedInfo;
  poolToken: TokenInfo;
};

const PoolUI = ({
  info: poolInfo,
  aggregatedInfo: poolAggregatedInfo,
  poolToken,
}: PoolUIProps) => {
  const { aggregatedInfo: reliquaryAggregatedInfo, rewardToken } =
    useReliquary();
  const poolUiInfo = poolTokenToUIInfo[poolToken.address];
  const apr = computeApr(
    poolAggregatedInfo.balanceUsd!,
    reliquaryAggregatedInfo.rewardRateUsd,
    poolAggregatedInfo.rewardShare,
  );
  return (
    <div className="flex flex-col bg-gray-800 p-4">
      <p className="text-center bg-blue-800">{poolUiInfo.displayName}</p>
      <p>Reward Alloc: {(100 * poolAggregatedInfo.rewardShare).toFixed(2)} %</p>
      <p>Reward Alloc Points: {poolInfo.allocPoint}</p>
      <p>
        TVL: ${poolAggregatedInfo.balanceUsd} (
        {formatUnits(poolAggregatedInfo.balance, poolToken.decimals)}{" "}
        {poolToken.symbol})
      </p>
      <p>Average APR: {apr} %</p>
      {poolInfo.levelsInfo
        .map((levelInfo, level) => ({
          levelInfo,
          aggregatedLevelInfo: poolAggregatedInfo.levelsInfo[level],
        }))
        .map(({ levelInfo, aggregatedLevelInfo }, level) => {
          const apr = computeApr(
            aggregatedLevelInfo.balanceUsd!,
            reliquaryAggregatedInfo.rewardRateUsd,
            aggregatedLevelInfo.absoluteRewardShare,
          );
          const rewardRate =
            SECONDS_PER_WEEK *
            aggregatedLevelInfo.absoluteRewardShare *
            reliquaryAggregatedInfo.rewardRate;
          const rewardRateUsd =
            SECONDS_PER_WEEK *
            aggregatedLevelInfo.absoluteRewardShare *
            reliquaryAggregatedInfo.rewardRateUsd;
          return (
            <p key={level}>
              <p className="text-center bg-gray-600">Level {level + 1}</p>
              <p>
                {" "}
                - Required maturity:{" "}
                {displayTimer(levelInfo.requiredMaturity, false)}
              </p>
              <p> - Multiplier x{levelInfo.multiplier}</p>
              <p>
                {" "}
                - Reward Alloc{" "}
                {(100 * aggregatedLevelInfo.absoluteRewardShare).toFixed(2)} %
              </p>
              <p>
                {" "}
                - Reward rate ${rewardRateUsd.toFixed(5)} / week (
                {rewardRate.toFixed(5)} {rewardToken.symbol})
              </p>
              <p> - TVL ${aggregatedLevelInfo.balanceUsd}</p>
              <p> - APR: {apr > 0 ? apr.toFixed(2) : "-"} %</p>
            </p>
          );
        })}
    </div>
  );
};

type CreatePrimeRelicModalProps = {
  owner: Address;
  isOpen: boolean;
  onClose: () => any;
};

const CreatePrimeRelicModal = ({
  owner,
  isOpen,
  onClose,
}: CreatePrimeRelicModalProps) => {
  const { reliquaryInfo, pools, poolsTokens } = useReliquary();

  const [selectedPoolIdx, setSelectedPoolIdx] = useState<number | undefined>(
    undefined,
  );

  const tokenInfo = useMemo(
    () =>
      selectedPoolIdx !== undefined ? poolsTokens[selectedPoolIdx] : undefined,
    [poolsTokens, selectedPoolIdx],
  );

  const PoolSelector = ({
    tokenAddress,
    selected,
    onSelect,
  }: {
    tokenAddress: Address;
    selected: boolean;
    onSelect: () => any;
  }) => {
    const poolUiInfo = poolTokenToUIInfo[tokenAddress];
    return (
      <div
        className={`${
          selected ? "bg-blue-600" : "bg-gray-800"
        } cursor-pointer p-2 w-full flex flex-col place-content-center`}
        onClick={onSelect}
      >
        <p className="text-center">{poolUiInfo.displayName}</p>
      </div>
    );
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose} className="min-w-[75%]">
      <p className="text-3xl text-center">Create New Relic</p>
      <p>Choose a pool:</p>
      <div className="flex md:flex-row sm:flex-col gap-2">
        {pools
          .filter((poolInfo) => poolInfo.token in poolTokenToUIInfo)
          .map((poolInfo, poolIdx) => (
            <PoolSelector
              key={poolIdx}
              tokenAddress={poolInfo.token}
              selected={selectedPoolIdx == poolIdx}
              onSelect={() => {
                setSelectedPoolIdx(poolIdx);
              }}
            />
          ))}
      </div>
      {tokenInfo === undefined ? (
        <></>
      ) : (
        <>
          <ExternalLink href={poolTokenToUIInfo[tokenInfo.address].url}>
            <Button className="w-full">Get Token</Button>
          </ExternalLink>
          <ApproveOrCreateAndDepositInput
            owner={owner}
            poolId={selectedPoolIdx}
            tokenInfo={tokenInfo}
            reliquaryAddress={reliquaryInfo.address}
          />
        </>
      )}
    </Modal>
  );
};

function AllRelics() {
  const { reliquaryInfo } = useReliquary();
  const { data: relicIds } = useRelicIds(reliquaryInfo.totalSupply);
  return relicIds && <_Relics relicIds={relicIds} />;
}

function _Relics({ relicIds }: { relicIds: number[] }) {
  const { reliquaryInfo, rewardToken } = useReliquary();
  const useRelicsResult = useRelics(relicIds);
  const relics = useMemo(() => useRelicsResult.data || [], [useRelicsResult]);
  const totalPendingRewards = useMemo(() => {
    return relics
      .map((relic) =>
        parseFloat(formatUnits(relic.pendingReward, rewardToken.decimals)),
      )
      .reduce((previousValue, currentValue) => previousValue + currentValue, 0);
  }, [relics, rewardToken]);

  return (
    <div>
      <p className="pb-8">
        Total pending rewards: {totalPendingRewards} /{" "}
        {formatUnits(reliquaryInfo.rewardBalance, rewardToken.decimals)}
      </p>
      <div className="grid lg:grid-cols-4 gap-x-2 gap-y-8">
        {relics.length ? (
          relics.map((relicInfo) => (
            <RelicInfoUI key={relicInfo.id} info={relicInfo} />
          ))
        ) : (
          <span>
            <i className="fa fa-refresh fa-spin"></i> Loading
          </span>
        )}
      </div>
    </div>
  );
}

export function ReliquaryUI() {
  const { data: reliquaryInfo } = useReliquaryInfo();
  const { data: rewardToken } = useToken({
    address: reliquaryInfo?.rewardToken,
  });
  const rewardTokenPrice = useTokenPrice(reliquaryInfo?.rewardToken);

  if (!reliquaryInfo || !rewardToken || !rewardTokenPrice) {
    return <div>Loading reliquary...</div>;
  }

  return (
    <ReliquaryContextProvider
      reliquaryInfo={reliquaryInfo}
      rewardToken={rewardToken}
      rewardTokenPrice={rewardTokenPrice}
    >
      <_ReliquaryUI />
    </ReliquaryContextProvider>
  );
}

function _ReliquaryUI() {
  const reliquary = useReliquary();
  const {
    reliquaryInfo,
    rewardToken,
    pools: reliquaryPools,
    poolsTokens,
    poolsAggregatedInfo,
    aggregatedInfo,
  } = reliquary;

  const queryClient = useQueryClient();

  const blockExplorerUrl = useBlockExplorerUrl();
  const [isReloadingReliquary, setIsReloadingReliquary] = useState(false);

  const { value: displayAllLevelsPerPool, toggle: toggleAllLevelsPerPool } =
    useBoolean(false);

  const onReload = useCallback(() => {
    console.log("Reloading reliquary");
    setIsReloadingReliquary(true);
    queryClient.invalidateQueries();
  }, [queryClient]);

  useEffect(() => {
    const timer = setInterval(() => {
      onReload();
    }, 60000);
    return () => clearInterval(timer);
  }, [onReload]);

  useEffect(() => {
    setIsReloadingReliquary(false);
  }, [reliquaryInfo]);

  const [showReliquaryDetails, setShowReliquaryDetails] = useState(false);
  const [showAllRelics, setShowAllRelics] = useState(false);

  const account = useAccount();

  return (
    <div>
      <Block>
        <p className="text-3xl">
          Reload{" "}
          <i
            className={`fa fa-refresh cursor-pointer ${
              isReloadingReliquary ? "fa-spin" : ""
            }`}
            onClick={() => {
              onReload();
            }}
          ></i>
        </p>
        <div className="flex flex-col gap-0 place-items-center">
          <p>
            Contract:{" "}
            <ExternalLink
              href={`${blockExplorerUrl}/address/${reliquaryInfo.address}`}
            >
              {reliquaryInfo.address}
            </ExternalLink>
          </p>
          <p>
            Reward token:{" "}
            <ExternalLink
              href={`${blockExplorerUrl}/token/${rewardToken.address}`}
            >
              {rewardToken.name} {rewardToken.symbol}
            </ExternalLink>
          </p>
          <p>TVL: ${aggregatedInfo.tvlUsd.toFixed(2)}</p>
          <p className="bg-blue-800 p-2">
            Average APR:{" "}
            {computeApr(
              aggregatedInfo.tvlUsd,
              aggregatedInfo.rewardRateUsd,
              1.0,
            ).toFixed(2)}{" "}
            %
          </p>
          <p>Reward rate:</p>
          <p>
            ${(aggregatedInfo.rewardRateUsd * SECONDS_PER_WEEK).toFixed(2)} /
            week ({(aggregatedInfo.rewardRate * SECONDS_PER_WEEK).toFixed(3)}{" "}
            {rewardToken.symbol})
          </p>
        </div>
        <div className="flex flex-row gap-3">
          <input
            type="checkbox"
            name="Show levels details"
            title="Show levels details"
            placeholder=""
            checked={displayAllLevelsPerPool}
            onChange={toggleAllLevelsPerPool}
          ></input>
          <label htmlFor="Show levels details">Show levels details</label>
        </div>
        <div className="flex md:flex-row sm:flex-col gap-2 w-full text-center">
          {reliquaryPools
            .map((poolInfo, poolIdx) => ({
              poolInfo,
              poolIdx,
              poolAggregatedInfo: poolsAggregatedInfo[poolIdx],
            }))
            .filter(
              ({ poolInfo, poolAggregatedInfo }) =>
                poolInfo.token in poolTokenToUIInfo &&
                poolAggregatedInfo.balanceUsd !== undefined,
            )
            .map(({ poolInfo, poolIdx, poolAggregatedInfo }) => (
              <div key={poolIdx} className="bg-gray-800 p-2 w-full">
                <p className="bg-blue-800">
                  {poolTokenToUIInfo[poolInfo.token].shortDisplayName}
                </p>
                <p>TVL: ${poolAggregatedInfo.balanceUsd!.toFixed(2)}</p>
                <p>
                  APR:{" "}
                  {computeApr(
                    poolAggregatedInfo.balanceUsd!,
                    aggregatedInfo.rewardRateUsd,
                    poolAggregatedInfo.rewardShare,
                  ).toFixed(2)}{" "}
                  %
                </p>
                {displayAllLevelsPerPool ? (
                  <>
                    {poolInfo.levelsInfo.map((levelInfo, level) => {
                      const apr = computeApr(
                        poolAggregatedInfo.levelsInfo[level].balanceUsd!,
                        aggregatedInfo.rewardRateUsd,
                        poolAggregatedInfo.levelsInfo[level]
                          .absoluteRewardShare,
                      );
                      return (
                        <p key={level}>
                          <p className="bg-gray-600">Level {level + 1}</p>
                          <p>
                            TVL: $
                            {poolAggregatedInfo.levelsInfo[
                              level
                            ].balanceUsd!.toFixed(2)}
                          </p>
                          <p>
                            APR: {apr > 0 ? apr.toFixed(2) : "-"}%{" "}
                            {apr > 100 ? "🔥" : ""}
                          </p>
                        </p>
                      );
                    })}
                  </>
                ) : (
                  <></>
                )}
              </div>
            ))}
        </div>
        <Button
          className="w-full"
          onClick={() => {
            setShowReliquaryDetails((value) => !value);
          }}
        >
          Show pools details
        </Button>
        {showReliquaryDetails ? (
          <div className="flex flex-col w-full gap-2">
            {reliquaryPools
              .map((poolInfo, poolIdx) => ({
                poolInfo,
                poolIdx,
                poolAggregatedInfo: poolsAggregatedInfo[poolIdx],
                poolToken: poolsTokens[poolIdx],
              }))
              .filter(({ poolInfo }) => poolInfo.token in poolTokenToUIInfo)
              .map(({ poolInfo, poolIdx, poolAggregatedInfo, poolToken }) => (
                <PoolUI
                  key={poolIdx}
                  info={poolInfo}
                  aggregatedInfo={poolAggregatedInfo}
                  poolToken={poolToken}
                />
              ))}
          </div>
        ) : (
          <></>
        )}
        <Button
          className="w-full"
          onClick={() => {
            setShowAllRelics((value) => !value);
          }}
        >
          Show all relics
        </Button>
        {showAllRelics && (
          <div className="flex flex-col w-full gap-2">
            <AllRelics />
          </div>
        )}
      </Block>
      {account.status === "connected" ? (
        <ReliquaryUserPosition address={account.address} />
      ) : (
        <Block>Connect your wallet to see your position</Block>
      )}
    </div>
  );
}

type ReliquaryUserPositionProps = {
  address: Address;
};

function ReliquaryUserPosition({ address }: ReliquaryUserPositionProps) {
  const { rewardToken, rewardTokenPrice, poolsTokens, poolsTokenPrices } =
    useReliquary();

  const [createIsOpen, setCreateIsOpen] = useState(false);
  const closeCreateModal = () => {
    setCreateIsOpen(false);
  };
  const openCreateModal = () => {
    setCreateIsOpen(true);
  };

  const ownerRelicIdsResult = useOwnerRelicIds(address);
  const ownerRelicsResult = useRelics(ownerRelicIdsResult.data || []);
  const ownerRelics = useMemo(
    () => ownerRelicsResult.data || [],
    [ownerRelicsResult],
  );

  const ownerAggregatedReliquaryInfo = useMemo(
    () =>
      aggregateOwnerReliquaryInfo(
        ownerRelics,
        rewardToken,
        rewardTokenPrice,
        poolsTokens,
        poolsTokenPrices,
      ),
    [ownerRelics, rewardToken, rewardTokenPrice, poolsTokens, poolsTokenPrices],
  );

  return (
    <>
      <Block>
        <p className="text-3xl">Your relics</p>
        <div className="flex flex-col gap-1 place-items-center w-full">
          <p>
            Total Value: ${ownerAggregatedReliquaryInfo.balanceUsd.toFixed(2)}
          </p>
          <p>
            Total Pending Rewards:{" "}
            {formatUnits(
              ownerAggregatedReliquaryInfo.pendingReward,
              rewardToken.decimals,
            )}{" "}
            {rewardToken.symbol} ($
            {ownerAggregatedReliquaryInfo.pendingRewardUsd.toFixed(2)})
          </p>
          <div className="flex md:flex-row sm:flex-col gap-2 w-full text-center">
            {ownerAggregatedReliquaryInfo.perPoolBalanceUsd.map(
              (balanceUsd, poolIdx) => (
                <div key={poolIdx} className="bg-gray-800 p-2 w-full">
                  <p className="bg-blue-800">
                    {
                      poolTokenToUIInfo[poolsTokens[poolIdx].address]
                        .shortDisplayName
                    }
                  </p>
                  <p>${balanceUsd.toFixed(2)}</p>
                </div>
              ),
            )}
          </div>
        </div>
        <Button className="w-full" onClick={openCreateModal}>
          Create
        </Button>
      </Block>
      {ownerRelics.map((relicInfo) => (
        <RelicUI key={relicInfo.id} info={relicInfo} owner={address} />
      ))}
      {createIsOpen && (
        <CreatePrimeRelicModal
          owner={address}
          isOpen={createIsOpen}
          onClose={closeCreateModal}
        />
      )}
    </>
  );
}
